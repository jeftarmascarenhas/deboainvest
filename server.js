'use strict';

let http =  require('http'),
	app = require('./config/express')
	;

const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';

http.createServer(app).listen(port, () => {
	console.log(`Servidor na porta ${port}`);
});