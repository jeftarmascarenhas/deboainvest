'use strict';

let express =  require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	path = require('path'),
	router = express.Router(),
	logger = require('morgan')
	;


app.set('views', './views');
app.set('view engine', 'ejs');

app.use(logger('dev'));
// app.use(express.static('./public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


app.get('/', (req, res) => {
	res.render('index', {title: 'essa Porra é crawler!'});
});


app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.render('error/404', {data: err});
});

// routes(app);

module.exports = app;