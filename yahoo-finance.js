'use strict';

let yahooFinance =  require('yahoo-finance');

yahooFinance.historical({
	symbol: 'CMIG3.sa',
	fields: ['s', 'y', 'x'],
	from: '2017-01-03',
	to: '2017-01-09'
}, (err, quotes) => {
	console.log(quotes);
});