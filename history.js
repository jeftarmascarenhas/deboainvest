'use strict';

const webdriver = require('selenium-webdriver'),
			By = webdriver.By,
			until = webdriver.until,
			firefox = require('selenium-webdriver/firefox'),
			chrome = require('selenium-webdriver/chrome'),
			cheerio = require('cheerio')
			;

const driver = new webdriver.Builder().forBrowser('firefox').build();


module.exports = function () {
	driver.get('http://www.fundamentus.com.br');
	driver.findElement(By.name('papel')).sendKeys('CMIG3');
	driver.findElement(By.css('input.botao')).click();
	driver.wait(until.elementLocated(By.css('table.w728')), 100000)
	.then(() => {
		let initSearch = () => {
			let elems = document.querySelector('body');
			let html = [];	
			Array.forEach([elems], (el) => {
				html.push(el.innerHTML);
			});

			return elems.innerHTML;
		};

		driver.executeScript(initSearch)
		.then(resp => {
			console.log(resp);
		});

	});
	
}
